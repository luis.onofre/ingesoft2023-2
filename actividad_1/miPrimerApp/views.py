from django.shortcuts import render, HttpResponse
from .models import *

# Create your views here.
def index(request):

    grupo1 = Estudiante.objects.filter(grupo = 1)
    grupo3 = Estudiante.objects.filter(grupo = 3)
    grupo4 = Estudiante.objects.filter(grupo = 4)
    todos = Estudiante.objects.all()

    edades = set()
    apellidos = set()

    for estudiante in todos:
        edades.add(estudiante.edad)
        apellidos.add(estudiante.apellidos)

    mismo_apellido = []
    misma_edad = []
    for edad in edades:
        iterator = filter(lambda x: x.edad == edad, todos)
        edad_data = {'edad': edad, 'estudiantes': list(iterator)} 
        misma_edad.append(edad_data) 

    for apellido in apellidos:
        iterator = filter(lambda x: x.apellidos == apellido, todos)
        apellido_data = {'apellido': apellido, 'estudiantes': list(iterator)} 
        mismo_apellido.append(apellido_data) 
    
    grupo3_misma_edad = []
    for edad in grupo3:
        iterator = filter(lambda x: x.edad == edad, grupo3)
        edad_data = {'edad': edad, 'estudiantes': list(iterator)} 
        grupo3_misma_edad.append(edad_data) 

    data = {
        'grupo1': grupo1,
        'grupo3': grupo3_misma_edad,
        'grupo4': grupo4,
        'mismoApellido': mismo_apellido,
        'mismaEdad': misma_edad,
        'todos': todos
    }

    return render(request, 'index.html', data)